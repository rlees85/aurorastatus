package io.bitservices.aurorastatus.worker

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.work.Worker
import androidx.work.WorkerParameters
import io.bitservices.aurorastatus.MainActivity
import io.bitservices.aurorastatus.R
import io.bitservices.aurorastatus.data.aurorawatch.Activity
import io.bitservices.aurorastatus.data.aurorawatch.SiteActivity
import io.bitservices.aurorastatus.model.NOAAParse
import io.bitservices.aurorastatus.net.AuroraWatch
import io.bitservices.aurorastatus.net.AuroraWatchInterface
import io.bitservices.aurorastatus.net.NOAA
import io.bitservices.aurorastatus.net.NOAAInterface
import retrofit2.Response
import java.time.ZonedDateTime

class CheckAuroraStatusWorker (context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    private val maxTries: Int = 3
    private val sharedPrefsFile: String = "alerts"
    private val sharedPrefsKey: String = "active"

    private var auroraWatchInterface: AuroraWatchInterface?=null
    private var noaaInterface: NOAAInterface?=null

    companion object {
        val tag: String = CheckAuroraStatusWorker::class.java.name
    }

    init {
        auroraWatchInterface = AuroraWatch.getAuroraWatchClient(context).create(AuroraWatchInterface::class.java)
        noaaInterface = NOAA.getNOAAClient().create(NOAAInterface::class.java)
    }

    override fun doWork(): Result {
        val allowRetry: Boolean = runAttemptCount < maxTries

        val auroraWatchActivityResponse: Response<SiteActivity>? = auroraWatchInterface?.fetchAuroraWatchActivity()?.execute()
        val auroraWatchActivityResult: SiteActivity? = auroraWatchActivityResponse?.body()

        var shouldAlert: Boolean = false

        val alertAuroraWatchActivity: Float
        var alertMagBz: Float? = null

        if (auroraWatchActivityResponse != null &&
            auroraWatchActivityResponse.isSuccessful &&
            auroraWatchActivityResponse.code() == 200 &&
            auroraWatchActivityResult != null) {
            val auroraWatchLastActivity: Activity? = auroraWatchActivityResult.latestActivity()

            if (auroraWatchLastActivity != null) {
                // We should have some valid data from AuroraWatch UK.
                alertAuroraWatchActivity = auroraWatchLastActivity.value

                // Check if we also need bZ information.
                if (alertAuroraWatchActivity >= 300f) {
                    // There is enough activity to alert regardless of bZ
                    shouldAlert = true
                } else if (alertAuroraWatchActivity >= 100f) {
                    // We need the bZ value as we may alert depending on that.
                    val noaaMagResponse: Response<List<List<String>>>? = noaaInterface?.fetchMag()?.execute()
                    val noaaMagResult: List<List<String>>? = noaaMagResponse?.body()

                    if (noaaMagResponse != null &&
                        noaaMagResponse.isSuccessful &&
                        noaaMagResponse.code() == 200 &&
                        noaaMagResult != null) {
                        try {
                            val noaaMagDataSet: NOAAParse = NOAAParse(noaaMagResult)
                            val noaaMagDirectionHeader: String = applicationContext.getString(R.string.noaa_header_mag_direction)
                            val (_: ZonedDateTime, noaaMagData: Map<String, String>) = noaaMagDataSet.getLatestData(
                                listOf(noaaMagDirectionHeader)
                            )
                            alertMagBz = noaaMagData[noaaMagDirectionHeader]!!.toFloat()
                            when(alertAuroraWatchActivity) {
                                in 200f..299f -> shouldAlert = alertMagBz <= 0f
                                in 100f..199f -> shouldAlert = alertMagBz <= -10f
                            }
                        } catch (err: Exception) {
                            // We failed to parse bZ data set and we can't alert without this information
                            return if (allowRetry) Result.retry() else Result.failure()
                        }
                    } else {
                        // We failed to get bZ and we can't alert without this information
                        return if (allowRetry) Result.retry() else Result.failure()
                    }
                }
            } else {
                // Failed to work out which activity was latest.
                return if (allowRetry) Result.retry() else Result.failure()
            }
        } else {
            // We could not even get Aurora Watch data, bZ data is pointless at this point, just fail the job.
            return if (allowRetry) Result.retry() else Result.failure()
        }

        val sp: SharedPreferences = applicationContext.getSharedPreferences(sharedPrefsFile, Context.MODE_PRIVATE)
        val isAlerting: Boolean = sp.getBoolean(sharedPrefsKey, false)

        if (shouldAlert) {
            if (!isAlerting) {
                // Alert Text
                val alertTextBuilder: StringBuilder = StringBuilder("Aurora may be visible! AuroraWatchUK activity: ")
                    .append(alertAuroraWatchActivity)
                    .append(" nT")

                if (alertMagBz != null) {
                    alertTextBuilder.append(", Magnetic direction: ")
                        .append(alertMagBz)
                        .append(" nT")
                }

                val alertText: String = alertTextBuilder.append(".")
                    .toString()

                // Notification Manager
                val nMan: NotificationManager =
                    applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                // Notification Channel
                val ncId: String =
                    applicationContext.packageName + "." + applicationContext.getString(R.string.notification_channel)
                val ncName: CharSequence = applicationContext.getString(R.string.app_name)
                val nc: NotificationChannel =
                    NotificationChannel(ncId, ncName, NotificationManager.IMPORTANCE_HIGH)
                nc.setShowBadge(true)
                nMan.createNotificationChannel(nc)

                // Notification Intent
                val nPendingIntent: PendingIntent = PendingIntent.getActivity(
                    applicationContext,
                    0,
                    Intent(applicationContext, MainActivity::class.java),
                    PendingIntent.FLAG_IMMUTABLE
                )

                // Notification
                val n: Notification = Notification.Builder(applicationContext, ncId)
                    .setContentTitle(applicationContext.getString(R.string.app_name))
                    .setContentText(alertText)
                    .setSmallIcon(R.drawable.ic_alert_icon)
                    .setColor(applicationContext.getColor(R.color.aurora))
                    .setContentIntent(nPendingIntent)
                    .build()

                nMan.notify(1, n)

                // Set the "we are now alerting" flag
                sp.edit()
                    .putBoolean(sharedPrefsKey, true)
                    .apply()
            }
        } else {
            if (isAlerting) {
                // Remove the "we are now alerting" flag
                sp.edit()
                    .putBoolean(sharedPrefsKey, false)
                    .apply()
            }
        }

        return Result.success()
    }
}