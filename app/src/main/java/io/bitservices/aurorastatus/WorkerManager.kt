package io.bitservices.aurorastatus

import android.content.Context
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import io.bitservices.aurorastatus.worker.CheckAuroraStatusWorker
import java.util.concurrent.TimeUnit

class WorkerManager() {
    companion object {
        fun startWork(context: Context) {
            val wm: WorkManager = WorkManager.getInstance(context)

            val alertConstraints: Constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val alertWork: PeriodicWorkRequest = PeriodicWorkRequest.Builder(
                CheckAuroraStatusWorker::class.java,
                1,
                TimeUnit.HOURS,
                30,
                TimeUnit.MINUTES
            )
                .setConstraints(alertConstraints)
                .build()

            wm.enqueueUniquePeriodicWork(
                CheckAuroraStatusWorker.tag,
                ExistingPeriodicWorkPolicy.UPDATE,
                alertWork
            )
        }

        fun stopWork(context: Context) {
            val wm: WorkManager = WorkManager.getInstance(context)

            wm.cancelAllWorkByTag(CheckAuroraStatusWorker.tag)
        }
    }
}