package io.bitservices.aurorastatus.model

import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class NOAAParse (rawData: List<List<String>>) {

    private val dataSet: List<List<String>>

    private val headerTimeTag: String = "time_tag"
    private val headers: Map<String, Int>

    private val timestampFormat: String = "yyyy-MM-dd HH:mm:ss'.'SSS"
    private val timestampZone: String = "UTC"

    init {
        if (rawData.isEmpty()) {
            throw Exception("Invalid data - nothing here, not even headers.")
        }

        val headerNamesRaw: List<String> = rawData[0]

        if (headerNamesRaw.isEmpty()) {
            throw Exception("No headers provided in headers list.")
        }

        val headerNamesMap: MutableMap<String, Int> = mutableMapOf()

        for (i in headerNamesRaw.indices) {
            headerNamesMap[headerNamesRaw[i]] = i
        }

        if (!headerNamesMap.containsKey(headerTimeTag)) {
            throw Exception("No timestamp header!")
        }

        headers = headerNamesMap
        dataSet = rawData.drop(1)
    }

    fun getLatestData(headerList: List<String>): Pair<ZonedDateTime, Map<String, String>> {
        for (header in headerList) {
            if (!hasHeader(header)) {
                throw Exception("Dataset has no header: $header")
            }
        }

        var latestTime: ZonedDateTime? = null
        val latestValues: MutableMap<String, String> = mutableMapOf()

        for (dataRecord in dataSet) {
            val recordTime: ZonedDateTime = ZonedDateTime.parse(dataRecord[headers[headerTimeTag]!!], DateTimeFormatter.ofPattern(timestampFormat).withZone(ZoneId.of(timestampZone)))
            if (latestTime == null || recordTime.isAfter(latestTime)) {
                latestTime = recordTime

                for (header in headerList) {
                    latestValues[header] = dataRecord[headers[header]!!]
                }
            }
        }

        if (latestTime == null || latestValues.keys.isEmpty()) {
            throw Exception("No data in data set!")
        }

        return Pair<ZonedDateTime, Map<String, String>>(latestTime, latestValues)
    }

    private fun hasHeader(header: String): Boolean {
        return headers.containsKey(header)
    }
}