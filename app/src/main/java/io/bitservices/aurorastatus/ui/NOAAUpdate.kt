package io.bitservices.aurorastatus.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.bitservices.aurorastatus.net.NOAA
import io.bitservices.aurorastatus.net.NOAAInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NOAAUpdate {
    private var noaaUpdateInterface: NOAAInterface?=null

    init {
        noaaUpdateInterface = NOAA.getNOAAClient().create(NOAAInterface::class.java)
    }

    fun getSolarWind():LiveData<List<List<String>>?> {
        val data = MutableLiveData<List<List<String>>?>()

        noaaUpdateInterface?.fetchSolarWind()?.enqueue(object : Callback<List<List<String>>>{

            override fun onFailure(call: Call<List<List<String>>>, t: Throwable) {
                data.value = null
            }

            override fun onResponse(
                call: Call<List<List<String>>>,
                response: Response<List<List<String>>>
            ) {
                val res = response.body()
                if (response.code() == 200 && res != null) {
                    data.value = res
                } else {
                    data.value = null
                }
            }
        })

        return data
    }

    fun getMag():LiveData<List<List<String>>?> {
        val data = MutableLiveData<List<List<String>>?>()

        noaaUpdateInterface?.fetchMag()?.enqueue(object : Callback<List<List<String>>>{

            override fun onFailure(call: Call<List<List<String>>>, t: Throwable) {
                data.value = null
            }

            override fun onResponse(
                call: Call<List<List<String>>>,
                response: Response<List<List<String>>>
            ) {
                val res = response.body()
                if (response.code() == 200 && res != null) {
                    data.value = res
                } else {
                    data.value = null
                }
            }
        })

        return data
    }
}
