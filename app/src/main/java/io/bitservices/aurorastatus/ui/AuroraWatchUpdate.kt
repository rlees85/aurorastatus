package io.bitservices.aurorastatus.ui

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.bitservices.aurorastatus.data.aurorawatch.SiteActivity
import io.bitservices.aurorastatus.net.AuroraWatch
import io.bitservices.aurorastatus.net.AuroraWatchInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuroraWatchUpdate (context: Context) {
    private var auroraWatchInterface: AuroraWatchInterface?=null

    init {
        auroraWatchInterface = AuroraWatch.getAuroraWatchClient(context).create(AuroraWatchInterface::class.java)
    }

    fun getActivity():LiveData<SiteActivity?> {
        val data = MutableLiveData<SiteActivity?>()

        auroraWatchInterface?.fetchAuroraWatchActivity()?.enqueue(object : Callback<SiteActivity>{

            override fun onFailure(call: Call<SiteActivity>, t: Throwable) {
                data.value = null
            }

            override fun onResponse(
                call: Call<SiteActivity>,
                response: Response<SiteActivity>
            ) {
                val res = response.body()
                if (response.code() == 200 && res != null) {
                    data.value = res
                } else {
                    data.value = null
                }
            }
        })

        return data
    }
}
