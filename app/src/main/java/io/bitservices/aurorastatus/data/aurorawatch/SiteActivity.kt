package io.bitservices.aurorastatus.data.aurorawatch

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "site_activity", strict = false)
data class SiteActivity (
    @field:ElementList(name="activity", inline = true, required = true)
    @param:ElementList(name="activity", inline = true, required = true)
    val activityList: List<Activity>
) {
    fun latestActivity(): Activity? {
        var latestActivity: Activity? = null

        for (activity in activityList) {
            if (latestActivity == null || activity.zonedDateTime.isAfter(latestActivity.zonedDateTime)) {
                latestActivity = activity
            }
        }

        return latestActivity
    }
}