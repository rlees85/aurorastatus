package io.bitservices.aurorastatus.data.aurorawatch

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@Root(name = "activity", strict = false)
data class Activity @JvmOverloads constructor (
    @field:Element(name="datetime")
    @param:Element(name="datetime")
    private val datetime: String,

    private val timestampFormat: String = "yyyy-MM-dd'T'HH:mm:ssxx",
    val zonedDateTime: ZonedDateTime = ZonedDateTime.parse(datetime, DateTimeFormatter.ofPattern(timestampFormat)),
    val zonedDateTimeString: String = zonedDateTime.toInstant().atZone(ZoneId.systemDefault()).toString(),

    @field:Element(name="value")
    @param:Element(name="value")
    val value: Float
) {
}