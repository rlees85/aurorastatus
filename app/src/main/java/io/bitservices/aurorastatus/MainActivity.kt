package io.bitservices.aurorastatus

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.github.anastr.speedviewlib.SpeedView
import com.github.anastr.speedviewlib.components.Section
import io.bitservices.aurorastatus.data.aurorawatch.Activity
import io.bitservices.aurorastatus.data.aurorawatch.SiteActivity
import io.bitservices.aurorastatus.model.NOAAParse
import io.bitservices.aurorastatus.ui.AuroraWatchUpdate
import io.bitservices.aurorastatus.ui.NOAAUpdate
import java.time.ZonedDateTime

class MainActivity : AppCompatActivity() {
    private lateinit var auroraWatchUpdate: AuroraWatchUpdate
    private lateinit var noaaUpdate: NOAAUpdate

    private lateinit var svAuroraWatchActivity: SpeedView
    private lateinit var svSolarWindSpeed: SpeedView
    private lateinit var svSolarWindDensity: SpeedView
    private lateinit var svMagDirection: SpeedView
    private lateinit var svMagStrength: SpeedView

    private lateinit var tvAuroraWatchTime: TextView
    private lateinit var tvVersion: TextView

    @ColorInt
    private var errorColor: Int = 0
    @ColorInt
    private var gaugeColor: Int = 0
    @ColorInt
    private var textColor: Int = 0

    private var auroraWatchActivityRefreshing: Boolean = false
    private var noaaSolarRefreshing: Boolean = false
    private var noaaMagRefreshing: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(getColor(R.color.primaryDark)))

        // Setup gauge controls (colour bands, etc)
        svAuroraWatchActivity = findViewById<SpeedView>(R.id.svAuroraWatchActivity)
        svSolarWindSpeed = findViewById<SpeedView>(R.id.svSolarWindSpeed)
        svSolarWindDensity = findViewById<SpeedView>(R.id.svSolarWindDensity)
        svMagDirection = findViewById<SpeedView>(R.id.svMagDirection)
        svMagStrength = findViewById<SpeedView>(R.id.svMagStrength)

        svAuroraWatchActivity.clearSections()
        svAuroraWatchActivity.addSections(
            Section(0f, .1f, getColor(R.color.gaugeGreen), svAuroraWatchActivity.speedometerWidth),
            Section(.1f, .2f, getColor(R.color.gaugeYellow), svAuroraWatchActivity.speedometerWidth),
            Section(.2f, .4f, getColor(R.color.gaugeOrange), svAuroraWatchActivity.speedometerWidth),
            Section(.4f, 1f, getColor(R.color.gaugeRed), svAuroraWatchActivity.speedometerWidth)
        )

        svSolarWindSpeed.clearSections()
        svSolarWindSpeed.addSections(
            Section(0f, .333f, getColor(R.color.gaugeGreen), svSolarWindSpeed.speedometerWidth),
            Section(.333f, .50f, getColor(R.color.gaugeYellow), svSolarWindSpeed.speedometerWidth),
            Section(.50f, .666f, getColor(R.color.gaugeOrange), svSolarWindSpeed.speedometerWidth),
            Section(.666f, 1f, getColor(R.color.gaugeRed), svSolarWindSpeed.speedometerWidth)
        )

        svSolarWindDensity.clearSections()
        svSolarWindDensity.addSections(
            Section(0f, .25f, getColor(R.color.gaugeGreen), svSolarWindDensity.speedometerWidth),
            Section(.25f, .5f, getColor(R.color.gaugeYellow), svSolarWindDensity.speedometerWidth),
            Section(.5f, .75f, getColor(R.color.gaugeOrange), svSolarWindDensity.speedometerWidth),
            Section(.75f, 1f, getColor(R.color.gaugeRed), svSolarWindDensity.speedometerWidth)
        )

        svMagDirection.clearSections()
        svMagDirection.addSections(
            Section(0f, .25f, getColor(R.color.gaugeRed), svMagDirection.speedometerWidth),
            Section(.25f, .4166f, getColor(R.color.gaugeOrange), svMagDirection.speedometerWidth),
            Section(.4166f, .5833f, getColor(R.color.gaugeYellow), svMagDirection.speedometerWidth),
            Section(.5833f, 1f, getColor(R.color.gaugeGreen), svMagDirection.speedometerWidth)
        )
        svMagDirection.speedTo(0f,0)

        svMagStrength.clearSections()
        svMagStrength.addSections(
            Section(0f, .2f, getColor(R.color.gaugeGreen), svMagStrength.speedometerWidth),
            Section(.2f, .4f, getColor(R.color.gaugeYellow), svMagStrength.speedometerWidth),
            Section(.4f, .6f, getColor(R.color.gaugeOrange), svMagStrength.speedometerWidth),
            Section(.6f, 1f, getColor(R.color.gaugeRed), svMagStrength.speedometerWidth)
        )

        // Setup text fields
        tvAuroraWatchTime = findViewById<TextView>(R.id.tvAuroraWatchTime)
        tvVersion = findViewById<TextView>(R.id.tvVersion)
        tvVersion.text = BuildConfig.VERSION_NAME

        // Setup base colours
        errorColor = getColor(R.color.textColorError)
        gaugeColor = svAuroraWatchActivity.speedTextColor
        textColor = tvAuroraWatchTime.currentTextColor

        // Setup update clients
        auroraWatchUpdate = AuroraWatchUpdate(this)
        noaaUpdate = NOAAUpdate()

        // Get initial data
        refresh()
    }

    override fun onResume() {
        super.onResume()

        // Get initial data
        refresh()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_main, menu)

        val isRefreshing: Boolean = auroraWatchActivityRefreshing ||
                                    noaaSolarRefreshing ||
                                    noaaMagRefreshing

        val refreshButton: MenuItem = menu.findItem(R.id.mnuRefresh)
        refreshButton.setVisible(!isRefreshing)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.mnuRefresh -> refresh()
            R.id.mnuStart -> WorkerManager.startWork(this)
            R.id.mnuStop -> WorkerManager.stopWork(this)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun refresh() {
        // Track refresh status
        auroraWatchActivityRefreshing = true
        noaaSolarRefreshing = true
        noaaMagRefreshing = true
        invalidateOptionsMenu()

        // Kick off API calls
        val auroraWatchActivity: LiveData<SiteActivity?> = auroraWatchUpdate.getActivity()

        val noaaUpdateSolar: LiveData<List<List<String>>?> = noaaUpdate.getSolarWind()
        val noaaUpdateMag: LiveData<List<List<String>>?> = noaaUpdate.getMag()

        auroraWatchActivity.observe(this, Observer {
            auroraWatchActivityRefreshing = false
            invalidateOptionsMenu()

            if(it!=null) {
                val latestActivity: Activity? = it.latestActivity()
                if (latestActivity != null) {
                    tvAuroraWatchTime.text = latestActivity.zonedDateTimeString
                    tvAuroraWatchTime.setTextColor(textColor)
                    gaugeSet(svAuroraWatchActivity, latestActivity.value)
                } else {
                    tvAuroraWatchTime.text = "Failed to resolve last activity update."
                    tvAuroraWatchTime.setTextColor(errorColor)
                    gaugeError(svAuroraWatchActivity)
                }
            } else {
                tvAuroraWatchTime.text = "Error calling AuroraWatch."
                tvAuroraWatchTime.setTextColor(errorColor)
                gaugeError(svAuroraWatchActivity)
            }
        })

        noaaUpdateSolar.observe(this, Observer {
            noaaSolarRefreshing = false
            invalidateOptionsMenu()

            if(it!=null) {
                try {
                    val noaaSolarDataSet: NOAAParse = NOAAParse(it)
                    val noaaSolarSpeedHeader: String = getString(R.string.noaa_header_solar_speed)
                    val noaaSolarDensityHeader: String = getString(R.string.noaa_header_solar_density)
                    val (_: ZonedDateTime, noaaSolarData: Map<String, String>) = noaaSolarDataSet.getLatestData(
                        listOf(noaaSolarSpeedHeader, noaaSolarDensityHeader)
                    )

                    try {
                        gaugeSet(svSolarWindSpeed, noaaSolarData[noaaSolarSpeedHeader]!!.toFloat())
                    } catch (err: Exception) {
                        gaugeError(svSolarWindSpeed)
                    }

                    try {
                        gaugeSet(svSolarWindDensity, noaaSolarData[noaaSolarDensityHeader]!!.toFloat())
                    } catch (err: Exception) {
                        gaugeError(svSolarWindDensity)
                    }
                } catch (err: Exception) {
                    gaugeError(svSolarWindSpeed)
                    gaugeError(svSolarWindDensity)
                }
            } else {
                gaugeError(svSolarWindSpeed)
                gaugeError(svSolarWindDensity)
            }
        })

        noaaUpdateMag.observe(this, Observer {
            noaaMagRefreshing = false
            invalidateOptionsMenu()

            if(it!=null) {
                try {
                    val noaaMagDataSet: NOAAParse = NOAAParse(it)
                    val noaaMagDirectionHeader: String = getString(R.string.noaa_header_mag_direction)
                    val noaaMagStrengthHeader: String = getString(R.string.noaa_header_mag_strength)
                    val (_: ZonedDateTime, noaaMagData: Map<String, String>) = noaaMagDataSet.getLatestData(
                        listOf(noaaMagDirectionHeader, noaaMagStrengthHeader)
                    )

                    try {
                        gaugeSet(svMagDirection, noaaMagData[noaaMagDirectionHeader]!!.toFloat())
                    } catch (err: Exception) {
                        gaugeError(svMagDirection)
                    }

                    try {
                        gaugeSet(svMagStrength, noaaMagData[noaaMagStrengthHeader]!!.toFloat())
                    } catch (err: Exception) {
                        gaugeError(svMagStrength)
                    }
                } catch (err: Exception) {
                    gaugeError(svMagDirection)
                    gaugeError(svMagStrength)
                }
            } else {
                gaugeError(svMagDirection)
                gaugeError(svMagStrength)
            }
        })
    }

    private fun gaugeSet(sv: SpeedView, speed: Float) {
        sv.speedTextColor = gaugeColor
        sv.textColor = gaugeColor
        sv.unitTextColor = gaugeColor
        sv.speedTo(speed)
    }
    private fun gaugeError(sv: SpeedView) {
        sv.speedTextColor = errorColor
        sv.textColor = errorColor
        sv.unitTextColor = errorColor
    }
}
