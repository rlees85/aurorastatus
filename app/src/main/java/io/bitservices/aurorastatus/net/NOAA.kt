package io.bitservices.aurorastatus.net

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NOAA {
    companion object {
        private const val BASE_URL: String = "https://services.swpc.noaa.gov/"

        private var retrofit: Retrofit? = null

        fun getNOAAClient(): Retrofit {
            if (retrofit == null) {
                val gson = GsonBuilder()
                    .create()

                val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .build()

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }

            return retrofit!!
        }
    }
}