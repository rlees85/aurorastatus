package io.bitservices.aurorastatus.net

import retrofit2.Call
import retrofit2.http.GET

interface NOAAInterface {
    @GET("products/solar-wind/plasma-5-minute.json")
    fun fetchSolarWind(): Call<List<List<String>>>

    @GET("products/solar-wind/mag-5-minute.json")
    fun fetchMag(): Call<List<List<String>>>
}