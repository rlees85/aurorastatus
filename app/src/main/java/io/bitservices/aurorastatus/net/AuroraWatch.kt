package io.bitservices.aurorastatus.net

import android.content.Context
import io.bitservices.aurorastatus.BuildConfig
import io.bitservices.aurorastatus.R
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.util.concurrent.TimeUnit

class AuroraWatch {
    companion object {
        private const val BASE_URL: String = "https://aurorawatch-api.lancs.ac.uk/0.2/"

        private var retrofit: Retrofit? = null

        fun getAuroraWatchClient(context: Context): Retrofit {
            if (retrofit == null) {
                val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(Interceptor {
                        val request: Request = it.request().newBuilder()
                            .addHeader("Referer", context.getString(R.string.app_repo_url))
                            .addHeader("User-Agent", context.getString(R.string.app_name) + " " + BuildConfig.VERSION_NAME)
                            .build()
                        return@Interceptor it.proceed(request)
                    })
                    .build()

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build()
            }

            return retrofit!!
        }
    }
}