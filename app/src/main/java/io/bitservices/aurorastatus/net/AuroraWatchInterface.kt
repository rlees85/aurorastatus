package io.bitservices.aurorastatus.net

import io.bitservices.aurorastatus.data.aurorawatch.SiteActivity
import retrofit2.Call
import retrofit2.http.GET

interface AuroraWatchInterface {
    @GET("status/alerting-site-activity.xml")
    fun fetchAuroraWatchActivity(): Call<SiteActivity>
}