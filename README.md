# Aurora Status

An app to check conditions for Aurora activity. It will also alert when certain
built-in conditions are met.

I have had no success with other applications for this purpose so I have rolled
my own. It is tuned specifically to my requirements and I have absolutely no
intention of publishing this application on any app store.

Anyone is free to use this code at their own risk for whatever they wish but
since this application relies on third-party APIs and data please read the
acknowledgement sections below before doing so!

## Acknowledgements

### [AuroraWatch UK](https://aurorawatch.lancs.ac.uk/)
AuroraWatch UK have certain requests for using their API. These can be found at:
https://aurorawatch.lancs.ac.uk/api-info/.

If you wish to use this code yourself and especially if you plan to publish
such code to an app store please pay attention to:

#### Client identification
Currently this application is identifying itself with the `User-Agent` header
and sending the repository URL as the `Referrer` header. Please make sure you
update the string `app_repo_url`.

#### Interval
Make sure the refresh interval for AuroraWatch UK is respected.

#### Colour Thresholds
In this application the colours in the gauges do NOT reflect "official" alert
levels as prescribed by AuroraWatch UK. They are customised to by own personal
preference for how I want to use this app. If the code was to be taken and
subsequently published please **make sure the AuroraWatch Activity gauge has
colour thresholds as prescribed by AuroraWatch UK** since to someone who does
not know otherwise the gauge colour thresholds could imply a relationship to
AuroraWatch UK alert levels.

### [SWPC](https://www.swpc.noaa.gov/) from [NOAA](https://www.noaa.gov/)
Data is gathered from SWPC/NOAA. As far as I am aware there are no restrictions
to using their APIs.

### [SpeedView](https://github.com/anastr/SpeedView)
Special thanks for this open source gauge control.
